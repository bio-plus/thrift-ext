/*
 * Copyright 2016 Neumitra, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.apache.thrift.transport;

import java.net.Socket;
import java.security.Principal;

import javax.naming.InvalidNameException;
import javax.naming.ldap.LdapName;
import javax.naming.ldap.Rdn;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;

import org.apache.thrift.AuthenticationFailed;
import org.apache.thrift.AuthenticationHandler;
import org.apache.thrift.MissingCredentials;
import org.apache.thrift.PrincipalProvider;
import org.apache.thrift.protocol.TMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TAuthenticatingSocket extends TSocket implements PrincipalProvider {

  private boolean isAuthenticated;
  private final AuthenticationHandler authenticationHandler;
  final static Logger logger = LoggerFactory.getLogger(TAuthenticatingSocket.class);

  public TAuthenticatingSocket(Socket socket, AuthenticationHandler authenticationHandler) throws TTransportException {
    super(socket);
    this.authenticationHandler = authenticationHandler;
  }

  @Override
  public Principal getPrincipal(TMessage msg) throws MissingCredentials {
    String mcMessage;
    try {
      final SSLSession session = ((SSLSocket) this.getSocket()).getSession();
      final LdapName ln = new LdapName(session.getPeerPrincipal().getName());
      for (Rdn rdn : ln.getRdns()) {
        if (rdn.getType().equalsIgnoreCase("CN")) {
          return new Principal() {
            @Override
            public String getName() {
              return (String) rdn.getValue();
            }
          };
        }
      }
      mcMessage = "No credentials provided by client.";
    } catch (ClassCastException e) {
      mcMessage = "Failed to cast Socket -> SSLSocket; no SSL transport?";
    } catch (SSLPeerUnverifiedException e) {
      mcMessage = "No SSL Peer; client verification is required!";
    } catch (InvalidNameException e) {
      mcMessage = "Invalid SSL peer name. This should never happen!";
    }
    logger.debug("Authentication failed: {}", mcMessage);
    throw new MissingCredentials(mcMessage);
  }

  @Override
  public int read(byte[] buf, int off, int len) throws TTransportException {
    if (!isAuthenticated) {
      logger.debug("Checking authentication before reading...");
      try {
        authenticationHandler.apply(null, this);
        logger.debug("Authentication succeeded.");
      } catch (AuthenticationFailed e) {
        throw new TTransportException(9, e);
      }
      isAuthenticated = true;
    }
    return super.read(buf, off, len);
  }

}
