Java Thrift Extensions
======================

Both Authentication and Authorization are implemented on the Java side.

Authentication
--------------

Authentication can be provided via explicit protocols or implicitly
using modified transports on the server-side.

Protocol-level
~~~~~~~~~~~~~~

``TAuthenticatedBinaryProtocol`` and ``TAuthenticatedJSONProtocol``
are drop-in replacements for the normal protocols. As they change the
underlying protocols, both server and client must use the same type.

An example client:

.. code:: java

   TProtocolFactory clientFactory = new TAuthenticatedBinaryProtocol.Factory("auth-key-here");
   TProtocol protocol = clientFactory.getProtocol(transport);
   FooService.Client client = new FooService.Client(protocol);

   try {
     client.bar();
   } catch (TProtocolException e) {
     if (e.getType() == TAuthenticatedProtocol.AUTH_FAILED) {
       // ... Handle failure
     }
   }

On the server, simply use a different Factory constructor and specify
a function that returns a message when authentication fails.

.. code:: java

   TProtocolFactory serverFactory = new TAuthenticatedBinaryProtocol.Factory(new Function<String, Optional<String>>() {
      public Optional<String> apply(String t) {
        if (!t.equals("good-auth-key")) {
          return Optional.of("Bad key!");
        }
        return Optional.empty();
      }
    });
   serverFactory.getProtocol(transport);

Transport-level
~~~~~~~~~~~~~~~

There are two transports capable of supplying credentials.

The first is a modified SSL transport that uses the client
certificate's CommonName field as the Principal. To use it, use
``TAuthenticatingSSLTransportFactory`` and the ``newServerSocket``
method instead of the usual ``SSLTransportFactory.getServerSocket``;
the methods take the same arguments.

.. code:: java

   TAuthenticatingSSLTransportFactory tfactory = new TAuthenticatingSSLTransportFactory(
     new Function<Principal, Optional<String>>() {
       public Optional<String> apply(Principal p) {
         if (!p.getName().equals("client")) {
           return Optional.of("Bad key!");
         }
         return Optional.empty();
       }
   });
   TServerSocket sock = tfactory.newServerSocket(/* ... */);

The second is a servlet which takes the Principal from a specific
Cookie. While technically a server and not a transport, it constructs
a ``TServletTransport`` which handles the actual authentication.

.. code:: java

   HttpServlet servlet = new TAuthenticatingServlet(factory, new Function<Principal, Optional<String>>() {
     public Optional<String> apply(Principal p) {
       if (!p.getName().equals("test")) {
         return Optional.of("Bad key!");
       }
       return Optional.empty();
     }
   }, "authCookieNameHere");

Running the servlet is left as an exercise, though an example can be found
in ``CookieAuthenticatingTest.java``.


Authorization
-------------

Authorization requires that credentials be provided by one of the
methods described above. If no credentials are provided, a
``TException`` will be returned.

Authorization is provided by ``TAuthorizingProcessorFactory``. Create
an instance of the factory with the appropriate base processor and
input protocol factory (necessary due to some message copying that has
to occur) and then use ``withSecureMethod()`` to add authorization
logic to service methods.

.. code:: java

   Handler handler = new Handler();
   TestService.Processor<Handler> processor = new TestService.Processor<Handler>(handler);
   TAuthorizingProcessorFactory factory = new TAuthorizingProcessorFactory(processor, new TBinaryProtocol.Factory());
   IAuthorizedMethod<TestService.getSecret_args> fn = new IAuthorizedMethod<TestService.getSecret_args>() {
      @Override
      public void check(TMessage msg, Principal peer, getSecret_args args) throws TException {
        if (!args.domain.equals("example2.com")) {
          throw new NotAuthorized();
        }
      }
    };
    factory.withSecureMethod(TestService.getSecret_args.class, fn);

If authorization fails for a given method call, throw any exception
that is defined as being thrown by the method in question. For
example, a method defined in the IDL as::

  void foo() throws NotAuthorized, ReallyNotAuthorized

would allow you to throw either ``NotAuthorized`` or
``ReallyNotAuthorized`` from ``IAuthorizedMethod.check()``. Throwing
any other exception will result in undefined behavior.


Passing Authentication
----------------------

If you're interested only in Authorization, use the convenience class
``PassingAuthentication`` in your protocol/transport which will
succeed so long as any credentials were provided.
