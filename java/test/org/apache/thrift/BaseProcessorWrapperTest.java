/*
 * Copyright 2016 Neumitra, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.apache.thrift;

import org.apache.thrift.protocol.TJSONProtocol1;
import org.apache.thrift.protocol.TMessage;
import org.apache.thrift.protocol.TMessageType;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.TMemoryBuffer;
import org.apache.thrift.transport.TTransport;

import com.neumitra.thrift.test.TestService.getSecret_args;
import com.neumitra.thrift.test.TestService.getSecret_result;

/**
 * A now-defunct test case that is left as an example of calling/receiving methods manually.
 *
 */
public class BaseProcessorWrapperTest {

  @SuppressWarnings("unused")
  private String readMessage(TProtocol p) throws TException {
    getSecret_result result = new getSecret_result();
    p.readMessageBegin();
    result.read(p);
    p.readMessageEnd();
    return result.getSuccess();
  }

  @SuppressWarnings("unused")
  private TProtocol writeMessage(String domain) throws TException {
    getSecret_args args = new getSecret_args(domain);
    TTransport itrans = new TMemoryBuffer(15);
    TProtocol iprot = new TJSONProtocol1(itrans);
    iprot.writeMessageBegin(new TMessage("getSecret", TMessageType.CALL, 1));
    args.write(iprot);
    iprot.writeMessageEnd();
    iprot.getTransport().flush();
    iprot.reset();
    return iprot;
  }

}
