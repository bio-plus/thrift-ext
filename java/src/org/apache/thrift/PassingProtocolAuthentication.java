/*
 * Copyright 2016 Neumitra, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.apache.thrift;

import java.util.Optional;
import java.util.function.Function;

/**
 * Authentication handler that succeeds so long as any credentials were provided.
 *
 */
public final class PassingProtocolAuthentication implements Function<String, Optional<String>> {

  @Override
  public Optional<String> apply(String t) {
    if (t == null || t.equals(""))
      return Optional.of("No credentials provided!");
    return Optional.empty();
  }

}
