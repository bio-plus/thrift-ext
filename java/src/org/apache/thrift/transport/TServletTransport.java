/*
 * Copyright 2016 Neumitra, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.apache.thrift.transport;

import java.io.IOException;
import java.security.Principal;
import java.util.Optional;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.thrift.MissingCredentials;
import org.apache.thrift.PrincipalProvider;
import org.apache.thrift.protocol.TMessage;

public class TServletTransport extends TIOStreamTransport implements PrincipalProvider {

  private final HttpServletRequest request;
  private final HttpServletResponse response;
  private final String cookieName;

  public TServletTransport(HttpServletRequest request, HttpServletResponse response, String cookieName) throws IOException {
    super(request.getInputStream(), response.getOutputStream());
    this.request = request;
    this.response = response;
    this.cookieName = cookieName;
  }


  public TServletTransport(HttpServletRequest request, HttpServletResponse response) throws IOException {
    this(request, response, "auth");
  }

  public HttpServletRequest getRequest() {
    return request;
  }

  public HttpServletResponse getResponse() {
    return response;
  }

  @Override
  public Principal getPrincipal(TMessage msg) throws MissingCredentials {
    Optional<Cookie[]> cookies = Optional.ofNullable(getRequest().getCookies());
    if (cookies.isPresent() && cookieName != null) {
      for (Cookie c : cookies.get()) {
        if (c.getName().equals(cookieName)) {
          return new Principal() {

            @Override
            public String getName() {
              return c.getValue();
            }
          };
        }
      }
    }
    throw new MissingCredentials("No session provided!");
  }

}
