package org.apache.thrift.transport;

import java.util.Arrays;
import java.util.LinkedList;

public class SequenceTransport extends TTransport {

  private final LinkedList<TTransport> pendingTransports;
  private TTransport activeTransport;

  public SequenceTransport(TTransport... transports) {
    this.pendingTransports = new LinkedList<>(Arrays.asList(transports));
  }

  @Override
  public int read(byte[] buf, int off, int len) throws TTransportException {
    if (pendingTransports.isEmpty() && activeTransport == null) {
      throw new TTransportException(TTransportException.END_OF_FILE);
    } else if (activeTransport == null) {
      activeTransport = pendingTransports.removeFirst();
      if (!activeTransport.isOpen()) {
        activeTransport.open();
      }
    }

    try {
      return activeTransport.read(buf, off, len);
    } catch (TTransportException e) {
      if (e.getType() == TTransportException.END_OF_FILE) {
        if (activeTransport.isOpen()) {
          activeTransport.close();
        }
        activeTransport = null;
        return read(buf, off, len);
      }
      throw(e);
    }

  }

  @Override
  public void close() {
    if (activeTransport != null && activeTransport.isOpen()) {
      activeTransport.close();
    }
  }

  @Override
  public boolean isOpen() {
    return !pendingTransports.isEmpty() || (activeTransport != null && activeTransport.isOpen());
  }

  @Override
  public void open() throws TTransportException {
    pendingTransports.peek().open();
  }

  @Override
  public void write(byte[] buf, int off, int len) throws TTransportException {
    throw new TTransportException("Can't write to SequenceTransport!");
  }

}
