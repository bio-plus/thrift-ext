namespace java overmind.test

struct TestStruct {
  1: string group
}

exception NotAuthorized {
  1: string failedField
}

service TestService {
  string getSecret(1:string domain) throws (1:NotAuthorized na)
}
