/*
 * Copyright 2016 Neumitra, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

Thrift.TAuthenticatedJSONProtocol = Thrift.AuthenticatedProtocol = function(transport, apiKey, authnHandler) {
    this.tstack = [];
    this.tpos = [];
    this.transport = transport;
    this.apiKey = apiKey;
    this.authnHandler = authnHandler;
};
Thrift.inherits(Thrift.TAuthenticatedJSONProtocol, Thrift.Protocol, 'AuthenticatedProtocol');

Thrift.AuthenticatedProtocol.prototype.AUTH_ID = 5;

Thrift.AuthenticatedProtocol.prototype.constructor =

Thrift.AuthenticatedProtocol.prototype.writeMessageBegin = function(name, messageType, seqid) {
    this.tstack = [];
    this.tpos = [];
    var msg = [Thrift.Protocol.Version, '"' + name + '"', messageType, seqid];
    if (messageType === Thrift.MessageType.CALL || messageType === Thrift.MessageType.ONEWAY) {
	msg.splice(0, 1, this.AUTH_ID, '"' + this.apiKey + '"');
    }
    this.tstack.push(msg);
};

Thrift.AuthenticatedProtocol.prototype.readMessageBegin = function() {
    this.rstack = [];
    this.rpos = [];
    var r = {}

    if (typeof JSON !== 'undefined' && typeof JSON.parse === 'function') {
	this.robj = JSON.parse(this.transport.readAll());
    } else if (typeof jQuery !== 'undefined') {
	this.robj = jQuery.parseJSON(this.transport.readAll());
    } else {
	this.robj = eval(this.transport.readAll());
    }
    var version = this.robj.shift();

    if (version !== this.AUTH_ID) {
	r.fname = this.robj.shift();
	r.mtype = this.robj.shift();
	if (r.mtype === Thrift.MessageType.CALL || r.mtype === Thrift.MessageType.ONEWAY) {
	    throw 'Wrong authenticated protocol version: ' + version;
	}
    } else {
	var result;
	r.apiKey = this.robj.shift();
	if (typeof this.authnHandler === 'function') {
	    result = this.authnHandler(r.apiKey);
	}
	if (typeof result === 'string') {
	    throw 'Authentication failed: ' + result;
	}
	r.fname = this.robj.shift();
	r.mtype = this.robj.shift();
    }

    r.rseqid = this.robj.shift();

    this.rstack.push(this.robj.shift());

    return r;
};
