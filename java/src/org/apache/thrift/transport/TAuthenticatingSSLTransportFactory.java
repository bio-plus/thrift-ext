/*
 * Copyright 2016 Neumitra, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.apache.thrift.transport;

import java.net.InetAddress;

import org.apache.thrift.AuthenticationHandler;
import org.apache.thrift.PassingAuthentication;

public class TAuthenticatingSSLTransportFactory extends TSSLTransportFactory {

  private final AuthenticationHandler authenticationHandler;

  public TAuthenticatingSSLTransportFactory() {
    this(new PassingAuthentication());
  }

  public TAuthenticatingSSLTransportFactory(AuthenticationHandler authenticationHandler) {
    this.authenticationHandler = authenticationHandler;
  }

  public TServerSocket newServerSocket(int port) throws TTransportException {
    TServerSocket sock = TSSLTransportFactory.getServerSocket(port);
    return new TAuthenticatingServerSocket(sock.getServerSocket(), 0, authenticationHandler);
  }

  public TServerSocket newServerSocket(int port, int clientTimeout) throws TTransportException {
   TServerSocket sock = TSSLTransportFactory.getServerSocket(port, clientTimeout);
   return new TAuthenticatingServerSocket(sock.getServerSocket(), clientTimeout, authenticationHandler);
  }

  public TServerSocket newServerSocket(int port, int clientTimeout, boolean clientAuth, InetAddress ifAddress) throws TTransportException {
    TServerSocket sock = TSSLTransportFactory.getServerSocket(port, clientTimeout, clientAuth, ifAddress);
    return new TAuthenticatingServerSocket(sock.getServerSocket(), clientTimeout, authenticationHandler);
  }

  public TServerSocket newServerSocket(int port, int clientTimeout, InetAddress ifAddress, TSSLTransportParameters params) throws TTransportException {
    TServerSocket sock = TSSLTransportFactory.getServerSocket(port, clientTimeout, ifAddress, params);
    return new TAuthenticatingServerSocket(sock.getServerSocket(), clientTimeout, authenticationHandler);
  }
}
