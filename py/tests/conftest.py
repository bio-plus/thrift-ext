"""
Copyright 2016 Neumitra, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

import pytest
from StringIO import StringIO

from thrift.protocol import TBinaryProtocol
from thrift.server import TServer
from thrift_ext.protocol import TAuthenticatedBinaryProtocolFactory, TAuthenticatedJSONProtocolFactory
from test_service.TestService import Client, Processor


class TestHandler(object):
    def rootPassword(self):
        return 'admin'

def authn(key):
    if key != 'good-api-key':
        return 'Bad API key!'


@pytest.fixture(scope='module', params=['binary', 'json'])
def factories(request):
    if request.param == 'binary':
        return [TAuthenticatedBinaryProtocolFactory(authnHandler=authn),
                TAuthenticatedBinaryProtocolFactory(apiKey='good-api-key')]
    elif request.param == 'json':
        return [TAuthenticatedJSONProtocolFactory(authnHandler=authn),
                TAuthenticatedJSONProtocolFactory(apiKey='good-api-key')]


@pytest.fixture(scope='session')
def processor():
    handler = TestHandler()
    return Processor(handler)
