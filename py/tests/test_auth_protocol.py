"""
Copyright 2016 Neumitra, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

import pytest
from thrift.transport.TTransport import TMemoryBuffer
from thrift.protocol.TProtocol import TProtocolException
from test_service.TestService import Client, Processor


def test_auth_protocol_with_good_api_key(factories, processor):
    srv, cli = factories
    r = roundtrip(srv, cli, processor)
    assert r == { 'proc': True, 'result': 'admin'}


def test_auth_protocol_skipping_auth(factories, processor):
    srv, cli = factories
    srv.authHandler = None
    r = roundtrip(srv, cli, processor)
    assert r == { 'proc': True, 'result': 'admin'}


def test_auth_protocol_with_bad_api_key(factories, processor):
    srv, cli = factories
    cli.apiKey = 'bad-api-key'
    with pytest.raises(TProtocolException) as e:
        r = roundtrip(srv, cli, processor)
        assert 'Authentication failed' in e.value
        assert r == { 'proc': False, 'result': None}


def roundtrip(sf, cf, proc):
    res = {'proc': None, 'result': None}

    # Send to "server"
    itrans = TMemoryBuffer()
    iproto = cf.getProtocol(itrans)
    client = Client(iproto)
    client.send_rootPassword()

    # Receive from "client"; send response
    itrans = TMemoryBuffer(itrans.getvalue())
    iproto = sf.getProtocol(itrans)
    otrans = TMemoryBuffer()
    oproto = cf.getProtocol(otrans)
    result = proc.process(iproto, oproto)
    res['proc'] = result

    # Receive from "server"
    itrans = TMemoryBuffer(otrans.getvalue())
    iprot = cf.getProtocol(itrans)
    client = Client(iprot)
    result = client.recv_rootPassword()
    res['result'] = result

    return res
