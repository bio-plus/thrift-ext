/*
 * Copyright 2016 Neumitra, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.apache.thrift;

import static org.junit.Assert.assertEquals;

import java.net.InetAddress;
import java.security.Principal;

import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.protocol.TMessage;
import org.apache.thrift.server.TServer;
import org.apache.thrift.server.TThreadPoolServer;
import org.apache.thrift.transport.TAuthenticatingSSLTransportFactory;
import org.apache.thrift.transport.TSSLTransportFactory;
import org.apache.thrift.transport.TSSLTransportFactory.TSSLTransportParameters;
import org.apache.thrift.transport.TServerSocket;
import org.apache.thrift.transport.TTransport;
import org.apache.thrift.transport.TTransportException;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExternalResource;

import com.neumitra.thrift.test.NotAuthorized;
import com.neumitra.thrift.test.TestService;
import com.neumitra.thrift.test.TestService.Client;
import com.neumitra.thrift.test.TestService.getSecret_args;

/**
 * Tests using TAuthorizingProcessorFactory with TAuthenticatingSSLTransportFactory to provided authorization.
 *
 */
public class SSLAuthorizationTest {

  @ClassRule
  public static ServerResource server = new ServerResource(new TBinaryProtocol.Factory(), true) {

    @Override
    public TServer getServer() throws TTransportException {
      TServerSocket sock = new TAuthenticatingSSLTransportFactory(new PassingAuthentication()).newServerSocket(port, 0,
          InetAddress.getLoopbackAddress(), makeParams("server"));
      return new TThreadPoolServer(new TThreadPoolServer.Args(sock).processorFactory(processorFactory));
    }

    @Override
    public TTransport getClient(boolean shouldAuthenticate) throws TTransportException {
      return TSSLTransportFactory.getClientSocket("localhost", port, 0, makeParams(shouldAuthenticate ? "client"
          : "server"));
    }

    private TSSLTransportParameters makeParams(String end) {
      TSSLTransportParameters params = new TSSLTransportParameters();
      params.setTrustStore(getClass().getClassLoader().getResource("server/truststore.jks").getPath(), "secret",
          "SunX509", "JKS");
      params.setKeyStore(getClass().getClassLoader().getResource(end + "/keystore.jks").getPath(), "secret", "SunX509",
          "JKS");
      params.requireClientAuth(true);
      return params;
    }

  };

  Client client;

  @Rule
  public ExternalResource clientResource = new ExternalResource() {
    @Override
    protected void before() throws Throwable {
      client = server.connectClient(true);
    }
  };

  @Before
  public void resetSecureMethods() {
    server.processorFactory.resetSecuredMethods();
  }

  @Test
  public void testProcessorWorksByDefault() throws NotAuthorized, TException {
    String result = client.getSecret("example.com");
    assertEquals(result, "s3cr3t");
  }

  @Test(expected = NotAuthorized.class)
  public void testProcessorThrowsNotAuthorizedWhenCheckFails() throws NotAuthorized, TException {
    IAuthorizedMethod<TestService.getSecret_args> fn = new IAuthorizedMethod<TestService.getSecret_args>() {
      @Override
      public void check(TMessage msg, Principal peer, getSecret_args args) throws TException {
        assert peer != null;
        if (!args.domain.equals("example2.com")) {
          throw new NotAuthorized("domain");
        }
      }
    };
    server.processorFactory.withSecureMethod(TestService.getSecret_args.class, fn);

    try {
        client.getSecret("example.com");    	
    } catch (NotAuthorized e) {
    	assertEquals("domain", e.failedField);
    	throw e;
    }
  }

  @Test
  public void testSuccessWhenCheckPasses() throws NotAuthorized, TException {
    IAuthorizedMethod<TestService.getSecret_args> fn = new IAuthorizedMethod<TestService.getSecret_args>() {
      @Override
      public void check(TMessage msg, Principal peer, getSecret_args args) throws TException {
        assert peer != null;
        if (!args.domain.equals("example.com")) {
          throw new NotAuthorized();
        }
        ;
      }
    };
    server.processorFactory.withSecureMethod(TestService.getSecret_args.class, fn);

    String result = client.getSecret("example.com");
    assertEquals(result, "s3cr3t");
  }

}
