package org.apache.thrift.transport;

import java.io.IOException;
import java.io.OutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TeeTransport extends TTransportDecorator {

  private final OutputStream branch;
  private final boolean closeBranch;
  private final static Logger logger = LoggerFactory.getLogger(TeeTransport.class);


  public TeeTransport(TTransport transport, OutputStream branch) {
    this(transport, branch, false);
  }

  public TeeTransport(TTransport transport, OutputStream branch, boolean closeBranch) {
    super(transport);
    this.branch = branch;
    this.closeBranch = closeBranch;
  }

  @Override
  public int read(byte[] buf, int off, int len) throws TTransportException {
    final int bytes = super.read(buf, off, len);
    if (bytes != -1) {
      try {
        branch.write(buf, off, len);
      } catch (IOException e) {
        throw new TTransportException(e);
      }
    }

    return bytes;
  }

  @Override
  public void close() {
    try {
      super.close();
    } finally {
      if (closeBranch) {
        try {
          branch.close();
        } catch (IOException e) {
          logger.error("Failed to close branch!", e);
        }
      }
    }

  }

}
