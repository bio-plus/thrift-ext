/*
 * Copyright 2016 Neumitra, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.apache.thrift.transport;

import java.net.ServerSocket;

import org.apache.thrift.AuthenticationHandler;
import org.apache.thrift.PassingAuthentication;

public class TAuthenticatingServerSocket extends TServerSocket {

  private final AuthenticationHandler authenticationHandler;

  public TAuthenticatingServerSocket(ServerSocket serverSocket, int clientTimeout) throws TTransportException {
    this(serverSocket, clientTimeout, new PassingAuthentication());
  }

  public TAuthenticatingServerSocket(ServerSocket serverSocket, int clientTimeout, AuthenticationHandler authenticationHandler) throws TTransportException {
    super(serverSocket, clientTimeout);
    this.authenticationHandler = authenticationHandler;
  }

  @Override
  protected TSocket acceptImpl() throws TTransportException {
    return new TAuthenticatingSocket(super.acceptImpl().getSocket(), authenticationHandler);
  }



}
