/*
 * Copyright 2016 Neumitra, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.apache.thrift.server;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.thrift.TException;
import org.apache.thrift.TProcessorFactory;
import org.apache.thrift.protocol.TJSONProtocol1;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.protocol.TProtocolFactory;
import org.apache.thrift.transport.TServletTransport;
import org.apache.thrift.transport.TTransport;

public class TAuthenticatingServlet extends HttpServlet {

  private static final long serialVersionUID = 1L;
  private final TProcessorFactory processorFactory;
  private final TProtocolFactory protocolFactory;
  private final Collection<Map.Entry<String, String>> customHeaders;
  private final String cookieName;

  public TAuthenticatingServlet(TProcessorFactory processorFactory) {
    this(processorFactory, new TJSONProtocol1.Factory(), "auth");
  }

  public TAuthenticatingServlet(TProcessorFactory processorFactory, String cookieName) {
    this(processorFactory, new TJSONProtocol1.Factory(), cookieName);
  }

  public TAuthenticatingServlet(TProcessorFactory processorFactory, TProtocolFactory protocolFactory, String cookieName) {
    super();
    this.processorFactory = processorFactory;
    this.protocolFactory = protocolFactory;
    this.customHeaders = new ArrayList<Map.Entry<String, String>>();
    this.cookieName = cookieName;
  }

  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    TTransport inTransport = null;
    TTransport outTransport = null;

    try {
      response.setContentType("application/x-thrift");

      for (Map.Entry<String, String> header : this.customHeaders) {
        response.addHeader(header.getKey(), header.getValue());
      }

      OutputStream out = response.getOutputStream();

      TServletTransport transport = new TServletTransport(request, response, cookieName);

      inTransport = transport;
      outTransport = transport;

      TProtocol inProtocol = protocolFactory.getProtocol(inTransport);
      TProtocol outProtocol = protocolFactory.getProtocol(outTransport);

      processorFactory.getProcessor(transport).process(inProtocol, outProtocol);
      out.flush();
    } catch (TException te) {
      response.sendError(HttpServletResponse.SC_FORBIDDEN, te.getMessage());
    }
  }

}
