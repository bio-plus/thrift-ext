Thrift Extensions
=================

This library implements some extensions to certain Thrift protocols.

Each supported language has implementations and tests in its
corresponding subfolder. Build-related commands should be run from the
root directory.

Authentication
--------------

Extensions to certain protocols have been created in support of
authentication. When using authenticated factories/protocols, clients
are configured to send an arbitrary API key (string) that can then be
validated by the server via an authentication callback.

The following table describes the extended protocol support for each
language and whether read (server) or write (client) is supported.

+-------------+---+----+---+----+
| Language    | Binary | JSON   |
+-------------+---+----+---+----+
|             | R | W  | R | W  |
+=============+===+====+===+====+
| Python      | x | x  | x | x  |
+-------------+---+----+---+----+
| Java        | x | x  | x | x  |
+-------------+---+----+---+----+
| Javascript  |   |    |   | x  |
+-------------+---+----+---+----+

Binary Format
~~~~~~~~~~~~~

The default binary protocol format contains a series of fields as::

  [type+version name seqid rest...]

Whereas the extended binary protocol looks like::

  [type+version api_key name seqid rest...]

The binary protocol uses bit masks to encode both a version number (1)
and a message type (0-5) into the first field. The authenticated
version simply uses a different starting number than the default. When
that version is encountered, we read/check the ``api_key`` before
letting the message through.

JSON Format
~~~~~~~~~~~

The JSON protocol format looks like::

  [VERSION name type seqid rest...]

Whereas the extended version looks like::

  [AUTH_VERSION api_key name type seqid rest...]

The primary difference is that it does not encode the type and version
into a single number, probably because JSON doesn't natively support
hex-formatted numbers.

License
-------

Licensed under the Apache License, Version 2.0.
See ``LICENSE`` for details.
