describe("Thrift.AuthenticatedProtocol", function() {
    var authnHandler = function(key) { if (key !== 'good-key') return 'bad!'; };
    var transport = new Thrift.Transport();
    var protocol = new Thrift.AuthenticatedProtocol(transport, 'good-key', authnHandler);
    var client = new TestServiceClient(protocol);

    beforeEach(function(){
	spyOn(transport, 'readAll').and.returnValue('[1, "rootPassword", 2, 0, {"0": {"str": "secret"}}]');
    });

    it("should send auth info", function() {
	var result = client.send_rootPassword();
	expect(result).toEqual('[5,"good-key","rootPassword",1,0,{}]');
    });

    it("should receive response correctly", function() {
	var result = client.recv_rootPassword();
	expect(result).toEqual('secret');
    });
});
