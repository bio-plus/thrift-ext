"""
Copyright 2016 Neumitra, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

from thrift.protocol.TProtocol import TProtocolException
from thrift.protocol.TBinaryProtocol import TBinaryProtocol
from thrift.protocol.TJSONProtocol import TJSONProtocol, VERSION as JSON_VERSION
from thrift.Thrift import TMessageType


# 0x70010000
AUTH_VERSION = -1879113728


class TAuthenticatedBinaryProtocol(TBinaryProtocol):
    def __init__(self, trans, apiKey, authnHandler):
        TBinaryProtocol.__init__(self, trans, strictRead=True, strictWrite=True)
        self.apiKey = apiKey
        self.authnHandler = authnHandler

    def writeMessageBegin(self, name, type, seqid):
        if type == TMessageType.CALL or type == TMessageType.ONEWAY:
            self.writeI32(AUTH_VERSION | type)
            self.writeString(self.apiKey)
        else:
            self.writeI32(TAuthenticatedBinaryProtocol.VERSION_1 | type)

        self.writeString(name)
        self.writeI32(seqid)

    def readMessageBegin(self):
        sz = self.readI32()
        assert sz < 0
        version = sz & TAuthenticatedBinaryProtocol.VERSION_MASK
        type = sz & TAuthenticatedBinaryProtocol.TYPE_MASK
        if (version != AUTH_VERSION and
            (type == TMessageType.CALL or type == TMessageType.ONEWAY) and
            self.authnHandler):
            raise TProtocolException(
                type=TProtocolException.UNKNOWN,
                message='Authentication is required!')
        elif version == AUTH_VERSION:
            apiKey = self.readString()
            if self.authnHandler:
                failed = self.authnHandler(apiKey)
                if failed:
                    raise TProtocolException(
                        type=TProtocolException.UNKNOWN,
                        message='Authentication failed: %s' % (failed,))

        elif version != TAuthenticatedBinaryProtocol.VERSION_1:
            raise TProtocolException(
                type=TProtocolException.BAD_VERSION,
                message='Bad version in readMessageBegin: %d' % (sz))
        name = self.readString()
        seqid = self.readI32()
        return (name, type, seqid)


class TAuthenticatedBinaryProtocolFactory:
    def __init__(self, apiKey='', authnHandler=None):
        self.apiKey = apiKey
        self.authnHandler = authnHandler
        self.authz_methods = {}

    def getProtocol(self, trans):
        prot = TAuthenticatedBinaryProtocol(trans, self.apiKey, self.authnHandler)
        return prot


class TAuthenticatedJSONProtocol(TJSONProtocol):
    def __init__(self, trans, apiKey, authnHandler):
        TJSONProtocol.__init__(self, trans)
        self.apiKey = apiKey
        self.authnHandler = authnHandler

    def writeMessageBegin(self, name, type, seqid):
        self.resetWriteContext()
        self.writeJSONArrayStart()
        if type == TMessageType.CALL or type == TMessageType.ONEWAY:
            self.writeJSONNumber(AUTH_VERSION)
            self.writeJSONString(self.apiKey)
        else:
            self.writeJSONNumber(JSON_VERSION)
        self.writeJSONString(name)
        self.writeJSONNumber(type)
        self.writeJSONNumber(seqid)

    def readMessageBegin(self):
        self.resetReadContext()
        self.readJSONArrayStart()
        i = self.readJSONInteger()
        if (i != AUTH_VERSION and
            (i == TMessageType.CALL or i == TMessageType.ONEWAY) and
            self.authnHandler):
            raise TProtocolException(
                type=TProtocolException.UNKNOWN,
                message='Authentication is required!')
        elif i == AUTH_VERSION:
            apiKey = self.readJSONString(False)
            if self.authnHandler:
                failed = self.authnHandler(apiKey)
                if failed:
                    raise TProtocolException(
                        type=TProtocolException.UNKNOWN,
                        message='Authentication failed: %s' % (failed,))
        elif i != JSON_VERSION:
            raise TProtocolException(TProtocolException.BAD_VERSION,
                                     "Message contained bad version.")
        name = self.readJSONString(False)
        typen = self.readJSONInteger()
        seqid = self.readJSONInteger()
        return (name, typen, seqid)


class TAuthenticatedJSONProtocolFactory:
  def __init__(self, apiKey='', authnHandler=None, strictRead=True, strictWrite=True):
      self.apiKey = apiKey
      self.authnHandler = authnHandler
      self.strictRead = strictRead
      self.strictWrite = strictWrite

  def getProtocol(self, trans):
      prot = TAuthenticatedJSONProtocol(trans, self.apiKey, self.authnHandler)
      return prot
