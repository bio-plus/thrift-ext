/*
 * Copyright 2016 Neumitra, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.apache.thrift;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.security.Principal;
import java.util.HashMap;
import java.util.Map;

import org.apache.thrift.protocol.TField;
import org.apache.thrift.protocol.TMessage;
import org.apache.thrift.protocol.TType;

/**
 * Represents a secure Thrift service method with logic for checking that a call
 * is authorized.
 *
 * @param <I>
 *          Thrift argument type as generated. Named as
 *          <code>{Service}.{Method}_args</code>
 */
public abstract class SecureMethod<I extends TBase<I, ? extends TFieldIdEnum> & Serializable & Cloneable & Comparable<I>>
    implements IAuthorizedMethod<I> {
  private final Class<I> argClass;
  private final Map<Class<? extends TException>, TFieldIdEnum> authFailedMap;

  public SecureMethod(Class<I> argClass) {
    this.argClass = argClass;
    this.authFailedMap = mapMethodResultFailureExceptions();
  }

  @SuppressWarnings({ "rawtypes", "unchecked" })
  private Map<Class<? extends TException>, TFieldIdEnum> mapMethodResultFailureExceptions() {
    final String clsName = "$" + getMethodName() + "_result";
    final String fieldsName = clsName + "$_Fields";
    Class<?> service = argClass.getEnclosingClass();

    Map<Class<? extends TException>, TFieldIdEnum> ret = new HashMap<>();
    for (Class c : service.getDeclaredClasses()) {
      if (c.getName().endsWith(clsName)) {
        Method fieldsMethod = null;
        try {
          for (Class cc : c.getDeclaredClasses()) {
            if (cc.getName().endsWith(fieldsName)) {
              fieldsMethod = cc.getDeclaredMethod("findByName", String.class);
              break;
            }
          }
        } catch (NoSuchMethodException | SecurityException e1) {
          throw new RuntimeException(e1);
        }
        for (Field f : c.getDeclaredFields()) {
          if (TException.class.isAssignableFrom(f.getType()) && !TException.class.equals(f.getType())) {
            Class<? extends TException> cc = (Class<? extends TException>) f.getType();
            try {
              ret.put(cc, (TFieldIdEnum) fieldsMethod.invoke(c, f.getName()));
            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
              throw new RuntimeException(e);
            }
          }
        }
        break;
      }
    }
    return ret;
  }

  public String getMethodName() {
    String name = argClass.getSimpleName();
    int idx = name.indexOf("_");
    return name.substring(0, idx);
  }

  public Class<I> getArgType() {
    return argClass;
  }

  public TField getFailedField(TSerializable exc) {
    TFieldIdEnum e = authFailedMap.get(exc.getClass());
    return new TField(e.getFieldName(), TType.STRUCT, e.getThriftFieldId());
  }

  public void check(TMessage msg, Principal peer, Object args) throws TException {
    check(msg, peer, getArgClass().cast(args));
  }

  @Override
  public abstract void check(TMessage msg, Principal peer, I args) throws TException;

  public Class<I> getArgClass() {
    return argClass;
  }

}
