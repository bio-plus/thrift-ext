/*
 * Copyright 2016 Neumitra, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.apache.thrift;

import java.io.Serializable;
import java.security.Principal;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.apache.thrift.protocol.TMessage;
import org.apache.thrift.protocol.TProtocolFactory;
import org.apache.thrift.transport.TTransport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A TProcessorFactory that adds authorization logic to service methods.
 *
 */
public class TAuthnzProcessorFactory extends TProcessorFactory {

  protected final TProcessor processor;
  protected final ConcurrentMap<String, SecureMethod<?>> securedMethods;
  protected AuthenticationHandler authnHandler;
  private final TProtocolFactory inProtocolFactory;
  private final static Logger logger = LoggerFactory.getLogger(TAuthnzProcessorFactory.class);

  public TAuthnzProcessorFactory(TProcessor processor, TProtocolFactory inProtocolFactory) {
    this(processor, PassingAuthentication.INSTANCE, inProtocolFactory);
  }

  public TAuthnzProcessorFactory(TProcessor processor, AuthenticationHandler authnHandler, TProtocolFactory inProtocolFactory) {
    super(processor);
    this.processor = processor;
    this.inProtocolFactory = inProtocolFactory;
    this.authnHandler = authnHandler;
    securedMethods = new ConcurrentHashMap<String, SecureMethod<?>>();
  }

  protected void setAuthenticationHandler(AuthenticationHandler ah) {
    this.authnHandler = ah;
  }

  public TAuthnzProcessorFactory withOptionalAuth() {
    this.authnHandler = OptionalAuthentication.INSTANCE;
    return this;
  }

  public Map<String, SecureMethod<?>> getSecuredMethods() {
    return securedMethods;
  }

  public void resetSecuredMethods() {
    securedMethods.clear();
  }

  public <I extends TBase<I, ? extends TFieldIdEnum> & Serializable & Cloneable & Comparable<I>> TAuthnzProcessorFactory withSecureMethod(
      final Class<I> argType, final IAuthorizedMethod<I> fn) {
    final String[] split = argType.getName().split("\\$");
    final String methodName = split[split.length - 1].substring(0, split[split.length - 1].indexOf("_"));
    logger.debug("Securing method {}", methodName);
    securedMethods.put(methodName, new SecureMethod<I>(argType) {
      @Override
      public void check(TMessage msg, Principal peer, I args) throws TException {
        fn.check(msg, peer, args);
      }

    });
    return this;
  }

  @Override
  public TProcessor getProcessor(TTransport trans) {
    return new TAuthnzProcessor(this.processor, inProtocolFactory, this.authnHandler, getSecuredMethods());
  }
}
