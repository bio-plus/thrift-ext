/*
 * Copyright 2016 Neumitra, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.apache.thrift;

import java.security.Principal;

import org.apache.thrift.protocol.TMessage;

/**
 * Optional interface for receiving a callback on authentication events.
 *
 */
public interface AuthenticationCallback {
  public void handleSuccess(TMessage msg, Principal p);
  public void handleFailure(TMessage msg, AuthenticationFailed exc);
}
