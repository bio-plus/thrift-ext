/*
 * Copyright 2016 Neumitra, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.apache.thrift;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ThreadLocalRandom;

import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.protocol.TProtocolFactory;
import org.apache.thrift.server.TServer;
import org.apache.thrift.transport.TTransport;
import org.apache.thrift.transport.TTransportException;
import org.junit.rules.ExternalResource;

import com.neumitra.thrift.test.TestService;
import com.neumitra.thrift.test.TestService.Client;

/**
 * Resource that manages the lifecycle of a Thrift server and provides client connections.
 *
 */
public abstract class ServerResource extends ExternalResource {

  public final int port;
  public final TAuthnzProcessorFactory processorFactory;
  private TServer server;
  private Optional<TServer> runningServer;
  private final TProtocolFactory clientProtocol;
  private final List<TTransport> clientTransports = new ArrayList<>();

  public ServerResource(TProtocolFactory clientProtocol) {
    this(clientProtocol, false);
  }

  public ServerResource(TProtocolFactory clientProtocol, boolean useAuthorization) {
    this.port = ThreadLocalRandom.current().nextInt(10000, 12000);
    Handler handler = new Handler();
    TestService.Processor<Handler> processor = new TestService.Processor<Handler>(handler);
    this.processorFactory = new TAuthnzProcessorFactory(processor, clientProtocol);
    if (!useAuthorization) {
      processorFactory.withOptionalAuth();
    }
    this.clientProtocol = clientProtocol;

  }

  public abstract TServer getServer() throws TTransportException;

  public abstract TTransport getClient(boolean shouldAuthenticate) throws TTransportException;

  @Override
  protected void before() throws Throwable {
    server = getServer();
    runningServer = Optional.of(server);
    Thread serverThread = new Thread(runnableServer(runningServer.get()));
    serverThread.start();
  }

  @Override
  protected void after() {
    if (runningServer.isPresent()) {
      runningServer.get().stop();
    }
    for (TTransport t : clientTransports) {
      t.close();
    }
  }

  private Runnable runnableServer(final TServer server) {
    return new Runnable() {

      @Override
      public void run() {
        server.serve();
      }

    };
  }

  public Client connectClient(boolean shouldAuthenticate) throws TTransportException {
    TTransport transport = getClient(shouldAuthenticate);
    clientTransports.add(transport);
    TProtocol protocol = clientProtocol.getProtocol(transport);
    return new TestService.Client(protocol);
  }
}
