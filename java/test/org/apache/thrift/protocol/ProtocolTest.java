/*
 * Copyright 2016 Neumitra, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.apache.thrift.protocol;

import static org.junit.Assert.assertTrue;

import java.lang.reflect.Constructor;
import java.security.Principal;

import org.apache.thrift.AuthenticationFailed;
import org.apache.thrift.AuthenticationHandler;
import org.apache.thrift.PrincipalProvider;
import org.apache.thrift.TAuthnzProcessorFactory;
import org.apache.thrift.TException;
import org.apache.thrift.TProcessor;
import org.apache.thrift.transport.TMemoryBuffer;
import org.apache.thrift.transport.TMemoryInputTransport;
import org.junit.Before;

import com.neumitra.thrift.test.AuthenticationError;
import com.neumitra.thrift.test.TestService;
import com.neumitra.thrift.test.TestService.Client;
import com.neumitra.thrift.test.TestService.Iface;

public abstract class ProtocolTest {
  protected TProtocolFactory protocolFactory;
  private TAuthnzProcessorFactory processorFactory;

  private static class Handler implements Iface {

    @Override
    public String getSecret(String domain) throws AuthenticationError, TException {
      return "admin";
    }

  }

  public abstract Class <? extends TProtocolFactory> factoryClass();

  @Before
  public void setUp() throws Exception {
    Constructor<? extends TProtocolFactory> constructor = factoryClass().getConstructor(String.class);
    protocolFactory = constructor.newInstance("good-api-key");
    TProcessor processor = new TestService.Processor<Handler>(new Handler());
    processorFactory = new TAuthnzProcessorFactory(processor,
        new AuthenticationHandler() {

          @Override
          public void apply(TMessage msg, PrincipalProvider pp) throws AuthenticationFailed {
            Principal p = pp.getPrincipal(msg);
            if (p == null || p.getName() == null || !p.getName().equals("good-api-key")) {
              throw new AuthenticationFailed("Bad key!");
            }
          }},
        protocolFactory);
  }

  public String roundTrip() throws TException {
    
    TMemoryBuffer itrans = new TMemoryBuffer(1);
    TProtocol iproto = protocolFactory.getProtocol(itrans);
    Client client = new TestService.Client(iproto);
    client.send_getSecret("test");

    TMemoryInputTransport itrans1 = new TMemoryInputTransport(itrans.getArray());
    TProcessor processor = processorFactory.getProcessor(itrans1);
    iproto = protocolFactory.getProtocol(itrans1);
    TMemoryBuffer otrans = new TMemoryBuffer(1);
    TProtocol oproto = protocolFactory.getProtocol(otrans);
    boolean result = processor.process(iproto, oproto);
    assertTrue(result);

    itrans1 = new TMemoryInputTransport(otrans.getArray());
    iproto = protocolFactory.getProtocol(itrans1);
    client = new TestService.Client(iproto);
    client.incSeq(); // Simulate previous send()
    String password = client.recv_getSecret();
    return password;
  }

}
