/*
 * Copyright 2016 Neumitra, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.apache.thrift;

import static org.junit.Assert.assertEquals;
import io.undertow.Handlers;
import io.undertow.Undertow;
import io.undertow.server.handlers.PathHandler;
import io.undertow.servlet.Servlets;
import io.undertow.servlet.api.DeploymentInfo;
import io.undertow.servlet.api.DeploymentManager;
import io.undertow.servlet.util.ImmediateInstanceFactory;

import java.io.IOException;
import java.security.Principal;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.HttpRequestRetryHandler;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.protocol.HttpContext;
import org.apache.thrift.protocol.TJSONProtocol;
import org.apache.thrift.protocol.TMessage;
import org.apache.thrift.server.TAuthenticatingServlet;
import org.apache.thrift.server.TServer;
import org.apache.thrift.transport.THttpClient;
import org.apache.thrift.transport.TTransport;
import org.apache.thrift.transport.TTransportException;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExternalResource;

import com.neumitra.thrift.test.NotAuthorized;
import com.neumitra.thrift.test.TestService;
import com.neumitra.thrift.test.TestService.Client;
import com.neumitra.thrift.test.TestService.getSecret_args;

/**
 * Tests using TAuthorizingProcessorFactory with TAuthenticatingServlet to provided authorization.
 *
 */
public class HTTPAuthorizationTest {

  @ClassRule
  public static ServerResource server = new ServerResource(new TJSONProtocol.Factory(), true) {

    @Override
    public TServer getServer() throws TTransportException {
      processorFactory.setAuthenticationHandler(PassingAuthentication.INSTANCE);
      HttpServlet servlet = new TAuthenticatingServlet(processorFactory);
      DeploymentInfo servletBuilder = Servlets
          .deployment()
          .setClassLoader(HTTPAuthorizationTest.class.getClassLoader())
          .setContextPath("/")
          .setDeploymentName("foo.war")
          .addServlets(
              Servlets.servlet("TestServiceServlet", TAuthenticatingServlet.class,
                  new ImmediateInstanceFactory<>(servlet)).addMapping("/*"));

      DeploymentManager manager = Servlets.defaultContainer().addDeployment(servletBuilder);
      manager.deploy();
      PathHandler path;
      try {
        path = Handlers.path(Handlers.redirect("/")).addPrefixPath("/", manager.start());
      } catch (ServletException e) {
        throw new TTransportException(e);
      }
      final Undertow server = Undertow.builder().addHttpListener(port, "localhost").setHandler(path).build();
      return new TServer(new TServer.Args(null)) {

        @Override
        public void serve() {
          server.start();
        }

        @Override
        public void stop() {
          server.stop();
        }
      };
    }

    @Override
    public TTransport getClient(boolean shouldAuthenticate) throws TTransportException {
      final int timeout = 2000;
      RequestConfig config = RequestConfig.custom()
          .setConnectTimeout(timeout)
          .setConnectionRequestTimeout(timeout)
          .setSocketTimeout(timeout).build();
      CookieStore cookies = new BasicCookieStore();
      if (shouldAuthenticate) {
        BasicClientCookie cookie = new BasicClientCookie("auth", "test");
        cookie.setDomain("localhost");
        cookies.addCookie(cookie);
      }
      HttpClient client = HttpClients.custom()
          .setDefaultCookieStore(cookies)
          .setDefaultRequestConfig(config)
          .setRetryHandler(new HttpRequestRetryHandler() {
            @Override
            public boolean retryRequest(IOException exception, int executionCount, HttpContext context) {
              return executionCount < 5;
            }})
          .build();
      return new THttpClient(String.format("http://localhost:%d", port), client);
    }
  };

  Client client;

  @Rule
  public ExternalResource clientResource = new ExternalResource() {

    @Override
    protected void before() throws Throwable {
      client = server.connectClient(true);
    }
  };

  @Before
  public void resetSecureMethods() {
    server.processorFactory.resetSecuredMethods();
  }

  @Test
  public void testProcessorWorksByDefault() throws NotAuthorized, TException, InterruptedException {
    String result = client.getSecret("example.com");
    assertEquals(result, "s3cr3t");
  }

  @Test(expected = NotAuthorized.class)
  public void testProcessorThrowsNotAuthorizedWhenCheckFails() throws NotAuthorized, TException {
    IAuthorizedMethod<TestService.getSecret_args> fn = new IAuthorizedMethod<TestService.getSecret_args>() {
      @Override
      public void check(TMessage msg, Principal peer, getSecret_args args) throws TException {
        assert peer != null;
        if (!args.domain.equals("example2.com")) {
          throw new NotAuthorized();
        }
      }
    };
    server.processorFactory.withSecureMethod(TestService.getSecret_args.class, fn);

    client.getSecret("example.com");
  }

  @Test
  public void testSuccessWhenCheckPasses() throws NotAuthorized, TException {
    IAuthorizedMethod<TestService.getSecret_args> fn = new IAuthorizedMethod<TestService.getSecret_args>() {
      @Override
      public void check(TMessage msg, Principal peer, getSecret_args args) throws TException {
        assert peer != null;
        if (!args.domain.equals("example.com")) {
          throw new NotAuthorized();
        }
        ;
      }
    };
    server.processorFactory.withSecureMethod(TestService.getSecret_args.class, fn);

    String result = client.getSecret("example.com");
    assertEquals(result, "s3cr3t");
  }

}
