from setuptools import setup

setup(
    name='thrift-ext',
    version='0.1',
    packages=['thrift_ext'],
    include_package_data=True,
    setup_requires=['pytest-runner'],
    install_requires=['thrift>=1.0.0-dev'],
    tests_require=['pytest'],
    license='Apache License 2.0',
    copyright='Neumitra, Inc',
    classifiers=[
        'Intended Audience :: Developers',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2',
        'Topic :: Software Development :: Libraries',
        'Topic :: System :: Networking',
        'License :: OSI Approved :: Apache Software License'
    ]
)
