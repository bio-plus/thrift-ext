/*
 * Copyright 2016 Neumitra, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.apache.thrift;

import static org.junit.Assert.assertEquals;

import java.net.InetAddress;

import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.protocol.TMessage;
import org.apache.thrift.server.TServer;
import org.apache.thrift.server.TThreadPoolServer;
import org.apache.thrift.transport.TAuthenticatingSSLTransportFactory;
import org.apache.thrift.transport.TSSLTransportFactory;
import org.apache.thrift.transport.TSSLTransportFactory.TSSLTransportParameters;
import org.apache.thrift.transport.TServerSocket;
import org.apache.thrift.transport.TTransport;
import org.apache.thrift.transport.TTransportException;
import org.junit.ClassRule;
import org.junit.Test;

import com.neumitra.thrift.test.TestService;
import com.neumitra.thrift.test.TestService.Client;
import com.neumitra.thrift.test.TestService.Iface;

/**
 * Tests using TAuthenticatingSSLTransportFactory to provide transparent authentication.
 *
 */
public class SSLAuthenticatingTest {

  @ClassRule
  public static ServerResource server = new ServerResource(new TBinaryProtocol.Factory()) {

    @Override
    public TServer getServer() throws TTransportException {
      TAuthenticatingSSLTransportFactory tfactory = new TAuthenticatingSSLTransportFactory(
          new AuthenticationHandler() {

            @Override
            public void apply(TMessage msg, PrincipalProvider pp) throws AuthenticationFailed {
              if (!pp.getPrincipal(msg).getName().equals("client")) {
                throw new AuthenticationFailed();
              }
            }

          });
      TServerSocket sock = tfactory.newServerSocket(port, 0, InetAddress.getLoopbackAddress(), makeParams("server"));
      TProcessor processor = new TestService.Processor<Iface>(new Handler());
      return new TThreadPoolServer(new TThreadPoolServer.Args(sock).processor(processor));
    }

    @Override
    public TTransport getClient(boolean shouldAuthenticate) throws TTransportException {
      return TSSLTransportFactory.getClientSocket("localhost", port, 0, makeParams(shouldAuthenticate ? "client" : "server"));
    }

    private TSSLTransportParameters makeParams(String end) {
      TSSLTransportParameters params = new TSSLTransportParameters();
      params.setTrustStore(getClass().getClassLoader().getResource("server/truststore.jks").getPath(), "secret",
          "SunX509", "JKS");
      params.setKeyStore(getClass().getClassLoader().getResource(end + "/keystore.jks").getPath(), "secret", "SunX509",
          "JKS");
      params.requireClientAuth(true);
      return params;
    }

  };

  @Test
  public void testAuthenticatedClient() throws TException {
    Client client = server.connectClient(true);
    String result = client.getSecret("example.com");
    assertEquals(result, "s3cr3t");
  }

  @Test(expected = TTransportException.class)
  public void testUnauthenticatedClient() throws TException {
    Client client = server.connectClient(false);
    client.getSecret("example.com");
  }

}
