/*
 * Copyright 2016 Neumitra, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.apache.thrift.protocol;

import java.security.Principal;
import java.util.Optional;

import org.apache.thrift.MissingCredentials;
import org.apache.thrift.PrincipalProvider;
import org.apache.thrift.TException;
import org.apache.thrift.transport.TTransport;

public class TAuthenticatedBinaryProtocol extends TProtocolDecorator implements PrincipalProvider {
  @SuppressWarnings("serial")
  public static class Factory implements TProtocolFactory {

    private final Optional<String> apiKey;

    public Factory(String apiKey) {
      this.apiKey = Optional.of(apiKey);
    }

    public TAuthenticatedBinaryProtocol getProtocol(TTransport trans) {
      return new TAuthenticatedBinaryProtocol(trans, this);
    }

  }

  private static final int AUTH_VERSION = 0x70010000;
  private static final int TYPE_MASK = 0x000000ff;

  private final Factory factory;
  private Principal principal;

  public TAuthenticatedBinaryProtocol(TTransport trans, Factory factory) {
    super(new TBinaryProtocol(trans));
    this.factory = factory;
  }

  @Override
  public void writeMessageBegin(TMessage message) throws TException {
    if (message.type == TMessageType.CALL || message.type == TMessageType.ONEWAY) {
      writeI32(TAuthenticatedBinaryProtocol.AUTH_VERSION | message.type);
      writeString(factory.apiKey.orElse(""));
    } else {
      writeI32(TBinaryProtocol.VERSION_1 | message.type);
    }
    writeString(message.name);
    writeI32(message.seqid);
  }

  @Override
  public TMessage readMessageBegin() throws TException {
    final int size = readI32();
    final int type = size & TYPE_MASK;
    final int version = size & TBinaryProtocol.VERSION_MASK;
    
    principal = null;

    if (version != AUTH_VERSION && (type == TMessageType.CALL || type == TMessageType.ONEWAY)) {
      throw new TProtocolException(9, "Authentication is required!");
    } else if (version == AUTH_VERSION) {
      final String auth = readString();
      principal = new Principal() {
        @Override
        public String getName() {
          return auth;
        }
      };
    }
    final String name = readString();
    final Integer seqid = readI32();
    final TMessage msg = new TMessage(name, (byte) type, seqid);
    return msg;
  }

  @Override
  public Principal getPrincipal(TMessage msg) throws MissingCredentials {
    if (principal == null) {
      throw new MissingCredentials("Credentials weren't provided or readMessageBegin() not called.");
    }
    return principal;
  }

}
