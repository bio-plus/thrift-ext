/*
 * Copyright 2016 Neumitra, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.apache.thrift;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.security.Principal;
import java.util.Map;
import java.util.Optional;

import org.apache.thrift.protocol.TField;
import org.apache.thrift.protocol.TMessage;
import org.apache.thrift.protocol.TMessageType;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.protocol.TProtocolException;
import org.apache.thrift.protocol.TProtocolFactory;
import org.apache.thrift.protocol.TProtocolUtil;
import org.apache.thrift.protocol.TStruct;
import org.apache.thrift.protocol.TType;
import org.apache.thrift.transport.SequenceTransport;
import org.apache.thrift.transport.TIOStreamTransport;
import org.apache.thrift.transport.TTransport;
import org.apache.thrift.transport.TTransportDecorator;
import org.apache.thrift.transport.TeeTransport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A TProcessor that confirms authorization to call a method.
 *
 */
public class TAuthnzProcessor implements TProcessor {

  private final TProcessor baseProcessor;
  private final TProtocolFactory protocolFactory;
  private final Map<String, SecureMethod<?>> secureMethods;
  private final AuthenticationHandler authnHandler;
  final static Logger logger = LoggerFactory.getLogger(TAuthnzProcessor.class);

  public TAuthnzProcessor(TProcessor baseProcessor, TProtocolFactory inProtocolFactory,
      AuthenticationHandler authnHandler, Map<String, SecureMethod<?>> secureMethods) {
    this.baseProcessor = baseProcessor;
    this.protocolFactory = inProtocolFactory;
    this.secureMethods = secureMethods;
    this.authnHandler = authnHandler;
  }

  private void notAuthorized(TMessage msg, TProtocol out, TField tField, TSerializable exc) throws TException {
    out.writeMessageBegin(new TMessage(msg.name, TMessageType.REPLY, msg.seqid));
    out.writeStructBegin(new TStruct(msg.name.concat("_result")));

    out.writeFieldBegin(tField);
    exc.write(out);
    out.writeFieldStop();
    out.writeStructEnd();
    out.writeFieldEnd();

    out.writeMessageEnd();
    out.getTransport().flush();
  }

  private static class AuthFailed {
    public final SecureMethod<?> sm;
    public final TSerializable exc;

    public AuthFailed(SecureMethod<?> sm, TSerializable exc) {
      this.sm = sm;
      this.exc = exc;
    }
  }

  /**
   * Check the method against all secured methods.
   *
   * @param name
   *          Name of the method
   * @param args
   *          Populated arguments for the method; must cast to method argument
   *          class given to the SecuredEndpoint of the same name.
   * @param principal
   * @return The endpoint that failed the check, IFF one matched by name and its
   *         check() method returned false.
   * @throws TException
   */
  private Optional<AuthFailed> checkMethod(TMessage msg, Object args, Principal principal) {
    Optional<SecureMethod<?>> sm = Optional.ofNullable(secureMethods.get(msg.name));
    if (sm.isPresent()) {
      try {
        sm.get().check(msg, principal, args);
        return Optional.empty();
      } catch (TException e) {
        return Optional.of(new AuthFailed(sm.get(), (TSerializable) e));
      }
    }
    logger.debug("Method {} does not require authorization.", msg.name);
    return Optional.empty();
  }

  private PrincipalProvider principalProvider(final TProtocol in) {
    return new PrincipalProvider() {
      @Override
      public Principal getPrincipal(TMessage msg) throws MissingCredentials {
        if (in instanceof PrincipalProvider) {
          return ((PrincipalProvider) in).getPrincipal(msg);
        } else if (in.getTransport() instanceof PrincipalProvider) {
          return ((PrincipalProvider) in.getTransport()).getPrincipal(msg);
        }
        throw new MissingCredentials("No credential-providing transport or protocol used!");
      }

    };
  }

  /**
   * Process the message after (maybe) checking authorization.
   *
   * This process is a bit convoluted in order to be compatible with the way
   * Thrift protocols drive transports. Since the protocols always know how many
   * bytes to expect, they don't generally rely on streams being closed to
   * indicate enough data has been read.
   *
   * We setup a TeeInputStream to record all bytes read from ``in`` for the
   * purposes of authorization. If authorization succeeds, we pass a new ``in``
   * protocol with a SequenceInputStream to the base processor that starts by
   * reading bytes already read for authorization, followed by whatever else may
   * be in the transport.
   *
   * In addition to this, we use the WrappingTransport class to make sure the
   * important PrincipalProvider interface isn't lost in all the
   * Transport/Stream conversions.
   *
   * If a method isn't marked as secure, processing will be performed as usual.
   */
  @SuppressWarnings({ "rawtypes", "unchecked" })
  @Override
  public boolean process(final TProtocol in, final TProtocol out) throws TException {
    logger.debug("About to process message...");
    final Map<String, ProcessFunction<?, ? extends TBase>> processMap = ((TBaseProcessor) baseProcessor)
        .getProcessMapView();

    final ByteArrayOutputStream branch = new ByteArrayOutputStream();
    TProtocol inCopy = protocolFactory.getProtocol(new WrappingTransport(new TeeTransport(in.getTransport(), branch, false), in.getTransport()));
    final TMessage msg = inCopy.readMessageBegin();
    logger.debug("Beginning authentication process for method: {}.", msg.name);
    final PrincipalProvider pp = principalProvider(inCopy);

    Principal principal;
    try {
      principal = pp.getPrincipal(msg);
      authnHandler.apply(msg, pp);
      if (authnHandler instanceof AuthenticationCallback) {
        ((AuthenticationCallback) authnHandler).handleSuccess(msg, principal);
      }
    } catch (AuthenticationFailed e1) {
      if (authnHandler instanceof AuthenticationCallback) {
        ((AuthenticationCallback) authnHandler).handleFailure(msg, e1);
      }
      throw new TProtocolException(9, e1);
    }

    logger.debug("Beginning authorization process for method: {}.", msg.name);
    ProcessFunction fn = processMap.get(msg.name);
    if (fn != null) { // Let the base processor handle this error case
      logger.debug("Checking Authorization for valid method: {}", msg.name);
      final TBase args = fn.getEmptyArgsInstance();
      args.read(inCopy);
      try {
        Optional<AuthFailed> res = checkMethod(msg, args, principal);
        if (res.isPresent()) {
          logger.warn("Authorization for method {} failed.", msg.name);
          notAuthorized(msg, out, res.get().sm.getFailedField(res.get().exc), res.get().exc);
          return true;
        }
      } catch (TException e) {
        TProtocolUtil.skip(inCopy, TType.STRUCT);
        inCopy.readMessageEnd();
        TApplicationException x = new TApplicationException(9, e.getMessage());
        out.writeMessageBegin(new TMessage(msg.name, TMessageType.EXCEPTION, msg.seqid));
        x.write(out);
        out.writeMessageEnd();
        out.getTransport().flush();
        return true;
      }
    }
    final byte[] branchBytes = branch.toByteArray();
    logger.debug("Authorization for method {} succeeded; allowing processing.", msg.name);
    logger.debug("Read {} bytes to authorize method {}.", branchBytes.length, msg.name);

    final TTransport newIn = new SequenceTransport(new TIOStreamTransport(new ByteArrayInputStream(branchBytes)), in.getTransport());
    return baseProcessor.process(protocolFactory.getProtocol(newIn), out);
  }

  static class WrappingTransport extends TTransportDecorator implements PrincipalProvider {

    final TTransport originalTransport;

    public WrappingTransport(TTransport newTransport, TTransport originalTransport) {
      super(newTransport);
      this.originalTransport = originalTransport;
    }

    @Override
    public Principal getPrincipal(TMessage msg) throws MissingCredentials {
      if (this.originalTransport instanceof PrincipalProvider) {
        return ((PrincipalProvider) originalTransport).getPrincipal(msg);
      }
      throw new MissingCredentials("No credentials provided by transport.");
    }

  }

}
