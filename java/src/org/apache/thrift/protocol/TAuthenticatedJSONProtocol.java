/*
 * Copyright 2016 Neumitra, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.apache.thrift.protocol;

import java.security.Principal;
import java.util.Optional;

import org.apache.thrift.MissingCredentials;
import org.apache.thrift.PrincipalProvider;
import org.apache.thrift.TException;
import org.apache.thrift.transport.TTransport;

public class TAuthenticatedJSONProtocol extends TJSONProtocol1 implements PrincipalProvider {
  @SuppressWarnings("serial")
  public static class Factory implements TProtocolFactory {

    private final Optional<String> apiKey;

    public Factory(String apiKey) {
      this.apiKey = Optional.ofNullable(apiKey);
    }

    public TAuthenticatedJSONProtocol getProtocol(TTransport trans) {
      return new TAuthenticatedJSONProtocol(trans, apiKey);
    }

  }

  private static final int AUTH_VERSION = 5;
  private static final int VERSION = 1;

  private final Optional<String> apiKey;
  private Principal principal;

  public TAuthenticatedJSONProtocol(TTransport trans, Optional<String> apiKey) {
    super(trans, false);
    this.apiKey = apiKey;
  }

  @Override
  public void writeMessageBegin(TMessage message) throws TException {
    super.writeJSONArrayStart();
    if (message.type == TMessageType.CALL || message.type == TMessageType.ONEWAY) {
      writeI32(AUTH_VERSION);
      writeString(apiKey.orElse(""));
    } else {
      writeI32(VERSION);
    }
    writeString(message.name);
    writeI32(message.type);
    writeI32(message.seqid);
  }

  @Override
  public TMessage readMessageBegin() throws TException {
    byte type;
    int version;
    String name;
    
    principal = null;

    super.readJSONArrayStart();

    final int i = readI32();
    if (i != AUTH_VERSION) {
      name = readString();
      type = (byte) readI32();
      version = i;
      if (type == TMessageType.CALL || type == TMessageType.ONEWAY) {
        throw new TProtocolException(9, "Authentication is required!");
      }
    } else {
      String apiKey = readString();
      version = i - 4;
      name = readString();
      type = (byte) readI32();
      principal = new Principal() {
        @Override
        public String getName() {
          return apiKey;
        }
      };
    }
    if (version != VERSION) {
      throw new TProtocolException(TProtocolException.BAD_VERSION, "Message contained bad version.");
    }
    final int seqid = (int) readI32();
    final TMessage msg = new TMessage(name, type, seqid);
    return msg;
  }

  @Override
  public Principal getPrincipal(TMessage msg) throws MissingCredentials {
    if (principal == null) {
      throw new MissingCredentials("Credentials weren't provided or readMessageBegin() not called.");
    }
    return principal;
  }

}
